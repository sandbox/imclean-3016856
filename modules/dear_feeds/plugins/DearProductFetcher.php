<?php

/**
 * Class DearProductFetcher.
 */
class DearProductFetcher extends FeedsFetcher {

  /**
   * Fetch content from a source and return it.
   *
   * @param \FeedsSource $source
   *
   * @return \DearProductFetcher
   */
  public function fetch(FeedsSource $source) {        
    
    $dear = dear_get_object(); // @todo this should be done better.    
    
    // Get all products, raw response format.
    $dear->listAllProducts();
    
    $content = $dear->getLastResponse();
    
    return new DearFetcherResult($content);
  }

}

/**
 * Base class for all fetcher results.
 */
class DearFetcherResult extends FeedsFetcherResult {
  /**
   * Override parent::sanitizeRaw().
   */
  public function sanitizeRaw($raw) {
    if (is_string($raw) && substr($raw, 0, 3) == pack('CCC', 0xef, 0xbb, 0xbf)) {
      $raw = substr($raw, 3);
    }
    return $raw;
  }
  
   /**
   * Override parent::sanitizeRawOptimized().
   */
  public function sanitizeRawOptimized(&$raw) {
    if (is_string($raw) && substr($raw, 0, 3) == pack('CCC', 0xef, 0xbb, 0xbf)) {
      $raw = substr($raw, 3);
    }
    return $raw;
  }
}
