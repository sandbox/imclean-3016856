<?php

class DearProductParser extends FeedsParser {
 
  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {    
    $json = $fetcher_result->getRaw();

    // Loop through products to make adjustments.
    foreach($json['Products'] as &$product) {      
      // Modify each price tier to include cents. DEAR only stores whole dollars.
      for ($x = 1; $x <= 10; $x++) {
        $product['PriceTier' . $x] = $product['PriceTier' . $x] * 100;
      }
    }

    $result = new FeedsParserResult($json['Products']);
    return $result;
  }

  /**
   * Return mapping sources.
   */
  public function getMappingSources() {
    // Get mapping fields from Product.    
    $dear = dear_get_object(); // @todo this should be done better.
    
    // Get a single product.
    $dear->listProducts(1,1);
    $product = $dear->getLastResponse();
    
    $fields = [];

    foreach ($product['Products'][0] as $key => $field) {
      $fields[$key] = [
        $key => [
          'name' => $key,
        ],
      ];
    }

    return parent::getMappingSources() + $fields;
  }

}
