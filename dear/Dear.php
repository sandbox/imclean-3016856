<?php

const DEAR_BASE_URL = 'inventory.dearsystems.com/ExternalApi/v2/';

class Dear {
  
  /**
   *
   * @var string Account ID
   */
  private $accountId;
  
  /**
   *
   * @var string API Key
   */
  private $apiKey;
 
  /**
   *
   * @var array Response from the last request.
   */
  private $lastResponse;
  
  /**
   * Set how many  times to retry on a 503 response.
   */
  public function setAccountId($account_id) {
    $this->accountId = $account_id;
  }

  /**
   * Set the API Key.
   */
  public function setApiKey($api_key) {
    $this->apiKey = $api_key;
  }  
  
  /**
   * Get the last received response.
   */
  public function getLastResponse() {
    return $this->lastResponse;
  }

  /**
   * Generate the URL.
   */
  public function getUrl($path) {
    // Build the URL.
    $url = 'https://' . DEAR_BASE_URL . $path;
    return $url;
  }  
  
  /**
   * Make a get request.
   */
  public function getRequest($path, $parameters = [], $raw = false) {
    // Get the url and make the request.
    $url = $this->getUrl($path);
    $options = [
      'method' => 'GET',
      'timeout' => 30,
      'headers' => [
        'Content-Type' => 'application/json',
        'api-auth-accountid' => $this->accountId,
        'api-auth-applicationkey' => $this->apiKey,
      ],
    ];
    
    // Add query parameters.
    $url = url($url, ['query' => $parameters]);

    // Make the request.
    $this->httpRequest($url, $options, $raw);
  }
  
  /**
   * POST request.
   */
  public function postRequest($path, $data = [], $raw = false) {
    // Get the url and make the request.
    $url = $this->getUrl($path);
    $encodedData = drupal_json_encode($data);
    $options = [
      'method' => 'POST',
      'data' => $encodedData,
      'timeout' => 30,
      'headers' => [
        'Content-Type' => 'application/json',
        'api-auth-accountid' => $this->accountId,
        'api-auth-applicationkey' => $this->apiKey,
      ],
    ];

    // Make the request.
    $this->httpRequest($url, $options, $raw);
  }

  /**
   * Make the request and handle the response.
   * 
   * @param string $url
   * @param array $options
   * @return boolean
   */
  public function httpRequest($url, $options, $raw) {

    $response = drupal_http_request($url, $options);

    // @TODO Handle the response better.
    if ($response->code != 200) {      
      $this->lastResponse = '';
      if (property_exists($response, 'data')) {
        // Check format of data
        if ($jsonResponse = json_decode($response->data)) {
          // Log DEAR's response.
          if (is_array($jsonResponse)) {
            watchdog('dear', 'Request failed for @url. Error code: @code Error message: @error', array(
              '@url' => $url,
              '@code' => $jsonResponse[0]->ErrorCode,
              '@error' => $jsonResponse[0]->Exception,
              ), WATCHDOG_ERROR);
          }
          else {
            watchdog('dear', 'Request failed for @url. Error code: @code Error message: @error', array(
              '@url' => $url,
              '@code' => $jsonResponse->ErrorCode,
              '@error' => $jsonResponse->Exception,
              ), WATCHDOG_ERROR);
          }
        }
        else {
          // Log DEAR's response.
          watchdog('dear', 'Request failed for @url. Error code: @code Error message: @error', array(
            '@url' => $url,
            '@code' => $response->code,
            '@error' => $response->data,
            ), WATCHDOG_ERROR);
        }
      }
      return FALSE;
    }
    else {
      if ($raw) {
        $this->lastResponse = $response->data;
      }
      else {
        $jsonResponse = drupal_json_decode($response->data);
        $this->lastResponse = $jsonResponse;
      }
      return TRUE;
    }

  }

  /**
   * List products.   
   */
  public function listProducts($limit = 100, $page = 1, $raw = false) {
    if ($this->getRequest('product',['Limit' => $limit, 'Page' => $page], $raw)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
  
  /**
   * List all products. There's 1000 product limit for each request.
   */
  public function listAllProducts($raw = false) {
    $page = 1;
    $this->listProducts(1000, $page, $raw);
    $products = $this->getLastResponse();
    $results = $products;
    if (count($products['Products']) == 1000) {
      while (count($products['Products']) == 1000) {
        $page++;
        $this->listProducts(1000, $page, $raw);
        $products = $this->getLastResponse();
        $results['Products'] = array_merge($results['Products'], $products['Products']);
      }
    }

    $this->lastResponse = $results;   

  }
  
  /**
   * Submit Sale to Dear    
   * 
   * @param array $sale DEAR Sale model 
   * @param array $sale_order DEAR Sale Order model 
   */
  public function submitSale($sale, $sale_order) {    
    $this->postRequest('sale', $sale);
    $sale_order['SaleID'] = $this->lastResponse['ID'];    
    $this->postRequest('sale/order', $sale_order);
    return $this->lastResponse;
  }
    

}
