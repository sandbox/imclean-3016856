<?php

/**
 * @file
 * DEAR Inventory admin pages.
 */

/**
 * Connection details form.
 */
function dear_connect_form() {
  $form['dear_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('DEAR Inventory Connection'),
  );
  
  $form['dear_config']['dear_account_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Account ID'),
    '#default_value' => variable_get('dear_account_id'),
    '#description' => t('The Account ID.'),
    '#required' => TRUE,
  );

  $form['dear_config']['dear_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#default_value' => variable_get('dear_key'),
    '#description' => t('The API Key.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
